const util = require("util")
const path = require("path")
var hfc = require('fabric-client');



hfc.setConfigSetting('network-connection-profile-path',path.join(__dirname, 'artifacts' ,"network-config.yaml"));
hfc.setConfigSetting('Org1-connection-profile-path',path.join(__dirname, 'artifacts', 'Org1.yaml'));
hfc.setConfigSetting('Org2-connection-profile-path',path.join(__dirname, 'artifacts', 'Org2.yaml'));
hfc.setConfigSetting('Org3-connection-profile-path',path.join(__dirname, 'artifacts', 'Org3.yaml'));

hfc.addConfigFile(path.join(__dirname, 'artifacts/config.json'));