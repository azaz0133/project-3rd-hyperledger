const { initServer } = require("./server")
const { createChannel, joinChannel } = require("./utils/channel")
const { getRegisteredUser } = require("./utils/helper")

require("./config")

const app = initServer()

app.post("/member/register", async (req, res) => {
	const username = req.body.username
	const orgName = req.body.orgName
	const result = await getRegisteredUser(username, orgName, true)
	res.status(201).json({
		result
	})
})

app.post("/channel/create",async (req, res) => {
	const { channelName, username, orgName } = req.body
	const result = await createChannel(channelName, username, orgName)
	console.log(result);
	res.status(201).json({
		result
	})
})

app.post("/channel/join", async (req, res) => {
	const { channelName, username, orgName, peers } = req.body
	console.log(req.body);
	const result = await joinChannel(channelName, peers, username, orgName)
	// console.log(result);
	res.status(201).json({
		result
	})
})

app.listen("8081", () => {
	console.log("server run on localhost:8081");
})

