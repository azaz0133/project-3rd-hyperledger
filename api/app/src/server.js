const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const init = () => {
    const app = express()
    app.use(cors({
        origin: "*"
    }))
    app.use(bodyParser.json())
    app.use(
        bodyParser.urlencoded({
            extended: false,
        })
    )
    
    return app
}

module.exports.initServer = init;