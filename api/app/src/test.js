var exec = require('child_process').exec;
// executes `pwd`
child = exec("docker exec cli peer channel create -o orderer.example.com:7050 -c example -f ./channel-artifacts/chanlnel.tx", function (error, stdout, stderr) {
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
        console.log('exec error: ' + error);
    }
});
// or more concisely
// var sys = require('sys')
// var exec = require('child_process').exec;
// function puts(error, stdout, stderr) { sys.puts(stdout) }
// exec("ls -la", puts);