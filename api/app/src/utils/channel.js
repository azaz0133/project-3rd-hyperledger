const helper = require('./helper.js');
const path = require('path')
const fs = require('fs')
const util = require("util")

async function createChannel(channelName, username, orgName) {
    const client = await helper.getClientForOrg(orgName, username)
    const channelConfigPath = path.join(__dirname, "..", "artifacts", `${channelName}.tx`)
    const envelope = fs.readFileSync(channelConfigPath)
    const channelConfig = client.extractChannelConfig(envelope)

    const signature = client.signChannelConfig(channelConfig)

    const request = {
        config: channelConfig,
        signatures: [signature],
        name: channelName,
        txId: client.newTransactionID(true),
        orderer: "orderer.example.com"
    }
    console.log(signature);
    try {
        const result = await client.createChannel(request)
        return result
    } catch (error) {
        console.log(error);
    }

}

var joinChannel = async function (channel_name, peers, username, org_name) {
    var error_message = null;
    var all_eventhubs = [];
    try {
        // first setup the client for this org
        var client = await helper.getClientForOrg(org_name, username);
        var channel = client.getChannel(channel_name);
        if (!channel) {
            let message = util.format('Channel %s was not defined in the connection profile', channel_name);
            throw new Error(message);
        }

        // next step is to get the genesis_block from the orderer,
        // the starting point for the channel that we want to join
        let request = {
            txId: client.newTransactionID(true) //get an admin based transactionID
        };
        let genesis_block = await channel.getGenesisBlock(request);

        // tell each peer to join and wait 10 seconds
        // for the channel to be created on each peer
        var promises = [];
        promises.push(new Promise(resolve => setTimeout(resolve, 10000)));
        let join_request = {
            targets: peers, //using the peer names which only is allowed when a connection profile is loaded
            txId: client.newTransactionID(true), //get an admin based transactionID
            block: genesis_block
        };
        let join_promise = channel.joinChannel(join_request);
        promises.push(join_promise);
        let results = await Promise.all(promises);

        // lets check the results of sending to the peers which is
        // last in the results array
        let peers_results = results.pop();
        // then each peer results
        for (let i in peers_results) {
            let peer_result = peers_results[i];
            if (peer_result instanceof Error) {
                error_message = util.format('Failed to join peer to the channel with error :: %s', peer_result.toString());
            } else if (peer_result.response && peer_result.response.status == 200) {
            } else {
                error_message = util.format('Failed to join peer to the channel %s', channel_name);
            }
        }
    } catch (error) {
        console.log(error);
        error_message = error.toString();
    }

    // need to shutdown open event streams
    all_eventhubs.forEach((eh) => {
        eh.disconnect();
    });

    if (!error_message) {
        let message = util.format(
            'Successfully joined peers in organization %s to the channel:%s',
            org_name, channel_name);
        // build a response to send back to the REST caller
        const response = {
            success: true,
            message: message
        };
        return response;
    } else {
        let message = util.format('Failed to join all peers to channel. cause:%s', error_message);
        // build a response to send back to the REST caller
        const response = {
            success: false,
            message: message
        };
        return response;
    }
};

module.exports.createChannel = createChannel
module.exports.joinChannel = joinChannel