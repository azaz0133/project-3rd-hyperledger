const path = require("path")
const util = require("util")
const hfc = require('fabric-client')


async function getClientForOrg(userOrg, username) {
    const config = '-connection-profile-path';

    const client = hfc.loadFromConfig(hfc.getConfigSetting('network' + config));
    client.loadFromConfig(hfc.getConfigSetting(userOrg + config));
    try {
        await client.initCredentialStores()

    } catch (error) {
        console.log(error.message);
    }

    if (username) {
        const user = await client.getUserContext(username, true)

        if (!user) {
            throw new Error(util.format("User wasn't found : ", username))
        }
    }

    return client;

}

async function getRegisteredUser(username, userOrg, isJson) {
    const client = await getClientForOrg(userOrg)
    let user = await client.getUserContext(username, true)

    if (user && user.isEnrolled()) {
        console.log("successful loaded member");
    } else {
        const admins = hfc.getConfigSetting("admins")
        const adminUserObj = await client.setUserContext({
            username: admins[0].username,
            password: admins[0].secret
        })

        const caClient = client.getCertificateAuthority()
        const secret = await caClient.register({
            enrollmentID: username,
            affiliation: userOrg.toLowerCase() + ".department1"
        }, adminUserObj)
        user = await client.setUserContext({username:username, password:secret});

    }

    if (user && user.isEnrolled()) {
        if (isJson && isJson === true) {
            var response = {
                success: true,
                secret: user._enrollmentSecret,
                message: username + ' enrolled Successfully',
            };
            return response;
        }
    } else {
        throw new Error('User was not enrolled ');
    }
}

exports.getRegisteredUser = getRegisteredUser;
exports.getClientForOrg = getClientForOrg;