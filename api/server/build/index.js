"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server");
const dotenv = require("dotenv");
const app = server_1.initServer();
dotenv.config();
console.log(process.env.CONFIG_ORG_PATH);
app.listen(app.get("port"), () => {
    console.log(`Server is running on port ${app.get('port')}`);
});
//# sourceMappingURL=index.js.map