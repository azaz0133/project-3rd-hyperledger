"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const model_1 = require("./model");
class CHyperledger {
    constructor() {
        this.generateConfigtxFromJson = (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                yield this.model.convertJsonToYaml(req.body.orgName, req.body.configtx);
                res.status(201).json({
                    statusCode: 201,
                    message: "created configtx file"
                });
            }
            catch (error) {
                res.status(500).json({
                    status: 500
                });
            }
        });
        this.addNewOrg = (req, res) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.model.addOrganizationToExistChannel();
        });
        this.model = new model_1.MHyperledger();
    }
}
exports.CHyperledger = CHyperledger;
//# sourceMappingURL=controller.js.map