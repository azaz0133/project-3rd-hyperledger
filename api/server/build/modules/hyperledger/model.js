"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const YAML = require("json-to-pretty-yaml");
const fs = tslib_1.__importStar(require("fs"));
const path = tslib_1.__importStar(require("path"));
const child_process_1 = require("child_process");
class MHyperledger {
    constructor() {
        this.convertJsonToYaml = (orgName, json) => new Promise((resolve, reject) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            const configtx = YAML.stringify(JSON.parse(JSON.stringify(json)));
            const location = path.join(__dirname, "..", "..", "..", "config_hyperledger", orgName);
            try {
                yield this.createFileConfigtx(location, orgName, configtx);
            }
            catch (error) {
                reject(error);
            }
        }));
        this.addOrganizationToExistChannel = () => new Promise((resolve, reject) => {
            const lo = path.join(__dirname, "..", "..", "..", "scripts", "add_org.sh");
            fs.chmodSync(lo, 777);
            child_process_1.exec(lo + " " + "lo", function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                console.log('stderr: ' + stderr);
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        });
        // public createChannelTX = () => new Promise((resolve,reject) => {
        // })
        this.createFileConfigtx = (locate, orgName, configtx) => new Promise((resolve, reject) => {
            if (!fs.existsSync(locate)) {
                fs.mkdirSync(locate);
            }
            fs.writeFile(locate + "/configtx.yaml", configtx, (err) => {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                if (!fs.existsSync(`${process.env.CONFIG_ORG_PATH}/${orgName}`)) {
                    fs.mkdirSync(`${process.env.CONFIG_ORG_PATH}/${orgName}`);
                }
                fs.copyFileSync(locate + "/configtx.yaml", `${process.env.CONFIG_ORG_PATH}/${orgName}/configtx.yaml`);
                resolve();
            });
        });
    }
}
exports.MHyperledger = MHyperledger;
//# sourceMappingURL=model.js.map