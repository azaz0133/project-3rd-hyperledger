"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const controller_1 = require("./controller");
function rHyperledger(router) {
    const controller = new controller_1.CHyperledger();
    router.post("/hyperledger/configtx/generate", controller.generateConfigtxFromJson);
    router.post("/hyperledger/channel/org/add", controller.addNewOrg);
}
exports.rHyperledger = rHyperledger;
//# sourceMappingURL=route.js.map