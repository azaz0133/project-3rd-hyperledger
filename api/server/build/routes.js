"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const route_1 = require("./modules/hyperledger/route");
function routes(router) {
    route_1.rHyperledger(router);
    return router;
}
exports.routes = routes;
//# sourceMappingURL=routes.js.map