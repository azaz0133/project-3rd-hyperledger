"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const express_1 = tslib_1.__importDefault(require("express"));
const body_parser_1 = tslib_1.__importDefault(require("body-parser"));
const cookie_parser_1 = tslib_1.__importDefault(require("cookie-parser"));
const cors_1 = tslib_1.__importDefault(require("cors"));
const routes_1 = require("./routes");
function initServer() {
    const app = express_1.default();
    app.use(cors_1.default({
        origin: "*"
    }));
    app.use(cookie_parser_1.default());
    app.use(body_parser_1.default.json());
    app.use(body_parser_1.default.urlencoded({
        extended: false,
    }));
    app.use("/api", routes_1.routes(express_1.default.Router({
        caseSensitive: true
    })));
    app.set("port", 8080 || process.env.PORT);
    return app;
}
exports.initServer = initServer;
//# sourceMappingURL=server.js.map