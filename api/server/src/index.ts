import { initServer } from "./server";
import dotenv =require('dotenv')
dotenv.config()
const app = initServer()
console.log(process.env.CONFIG_ORG_PATH);
app.listen(app.get("port"),() => {
    console.log(`Server is running on port ${app.get('port')}`)
})