import { model, Schema } from "mongoose";


export const OrganizationModel = model("Organization", new Schema({
    orgName: String,
    mspDir: String,
    orgID: String,
    policies: {
        type: Array
    },
    anchorPeers: {
        type: Array
    },
    peers: {
        type: Array
    }
}))
