import * as express from 'express'
import { MHyperledger } from './model';


export class CHyperledger {
    public model: MHyperledger

    constructor() {
        this.model = new MHyperledger()
    }

    public registerMember: express.RequestHandler = async (
        req: express.Request,
        res: express.Response | any
    ): Promise<void> => {
        const {
            username,
            orgName
        } = req.body
        try {
            const result = await this.model.registerMember(username, orgName)
            res.status(201).json(result)
        } catch (error) {
            res.boom.badRequest("sonething wrong") as any
        }
    }

    public generateConfigtxFromJson: express.RequestHandler = async (req: express.Request, res: express.Response) => {
        try {
            await this.model.convertJsonToYaml(req.body.orgName, req.body.configtx)
            res.status(201).json({
                statusCode: 201,
                message: "created configtx file"
            })
        } catch (error) {
            res.status(500).json({
                status: 500
            })
        }
    }

    public addNewOrg = async (
        req: express.Request,
        res: express.Response
    ) => {
        // this.model.addOrganizationToExistChannel()
    }
}