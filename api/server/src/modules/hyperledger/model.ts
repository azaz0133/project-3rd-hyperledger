import YAML = require('json-to-pretty-yaml')
import * as fs from 'fs'
import * as path from 'path'
import { exec } from 'child_process'
import Axios from 'axios'
import { HYPER_HOST } from '../../constants';
export class MHyperledger {

    public registerMember = (username, orgName) => new Promise(async (resolve, reject) => {
        try {
            const { data, status } = await Axios.post(HYPER_HOST + "/member/register", {
                username,
                orgName
            })
            console.log(status);
            console.log(data);
            resolve(data);

        } catch (error) {

        }
    })

    public createChannel = () => new Promise(async (resolve, reject) => {
        try {
            const { data, status } = await Axios.post(HYPER_HOST, {

            })
            if (status == 201) {
                if (data) {

                }
            }
        } catch (error) {

        }
    })


    public convertJsonToYaml = (orgName: string, json: any) => new Promise(async (resolve, reject) => {
        const configtx =
            YAML.stringify(JSON.parse(JSON.stringify(json)))
        const location: string = path.join(__dirname, "..", "..", "..", "config_hyperledger", orgName)
        try {
            await this.createFileConfigtx(location, orgName, configtx)
        } catch (error) {
            reject(error)
        }

    })

    public addOrganizationToExistChannel = (OrgMSP: string, org: string) => new Promise((resolve, reject) => {
        const lo = path.join(__dirname, "..", "..", "..", "scripts", "add_org.sh")
        fs.chmodSync(lo, 777)
        exec(lo + " " + "", function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
    })

    // public createChannelTX = () => new Promise((resolve,reject) => {

    // })

    private createFileConfigtx = (locate: string, orgName: string, configtx: any): Promise<any> => new Promise((resolve, reject) => {
        if (!fs.existsSync(locate)) {
            fs.mkdirSync(locate)
        }
        fs.writeFile(locate + "/configtx.yaml", configtx, (err) => {
            if (err) {
                console.log(err);
                reject(err)
            }
            if (!fs.existsSync(`${process.env.CONFIG_ORG_PATH}/${orgName}`)) {
                fs.mkdirSync(`${process.env.CONFIG_ORG_PATH}/${orgName}`)
            }
            fs.copyFileSync(locate + "/configtx.yaml", `${process.env.CONFIG_ORG_PATH}/${orgName}/configtx.yaml`)
            resolve()
        })
    })


}



