import { CHyperledger } from './controller';
import * as express from 'express';



export function rHyperledger(router: express.Router): void {
    const controller = new CHyperledger()
    router.post("/hyperledger/configtx/generate", controller.generateConfigtxFromJson)
    router.post("/hyperledger/channel/org/add", controller.addNewOrg)
    router.post("/hyperledger/member/register", controller.registerMember)
}