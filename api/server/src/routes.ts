import * as express from "express";
import { rHyperledger } from './modules/hyperledger/route';


export function routes(router: express.Router) {
    rHyperledger(router)
    return router;
}