import express from 'express'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import { routes } from './routes';

export function initServer(): express.Application {
    const app = express()
    app.use(cors({
        origin: "*"
    }))
    app.use(cookieParser())
    app.use(bodyParser.json())
    app.use(
        bodyParser.urlencoded({
            extended: false,
        }),
    )
    app.use(require("express-boom")())
    app.use("/api", routes(express.Router({
        caseSensitive: true
    })))

    app.set("port", 8080 || process.env.PORT)
    return app;
}