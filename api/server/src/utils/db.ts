import { createConnection, Connection } from 'mongoose'

export function initDatabase(): Connection {
    return createConnection(process.env.MONGO_HOST)
}