import { MHyperledger } from './../../../src/modules/hyperledger/model';
import chai from 'chai'



describe("add new member", () => {

    it("username: azaz0233 org: Org1", async () => {
        const result = await new MHyperledger().registerMember()
        chai.expect(result).to.be.an("object")
    })

})

describe("Channel Test", () => {
    it("create channel", async () => {
        const result = await new MHyperledger().createChannel()
        chai.expect(result).to.be.an("object")
    })
})