
# peer channel create -o orderer.example.com:7050 -c example -f ./channel-artifacts/channel.tx
 
# add new org section
export ORG=Org3MSP 
export FABRIC_CFG_PATH=./config/org3
./bin/configtxgen -printOrg Org3 > ./channel-artifacts/${ORG}.json
export FABRIC_CFG_PATH=/etc/hyperledger/fabric

peer channel fetch config config_block.pb -o orderer.example.com:7050 -c example
configtxlator proto_decode --input config_block.pb --type common.Block | jq .data.data[0].payload.data.config > config.json
jq -s '.[0] * {"channel_group":{"groups":{"Application":{"groups":{"'${ORG}'":.[1]}}}}}' config.json ./channel-artifacts/${ORG}.json > modified_config.json

configtxlator proto_encode --input config.json --type common.Config --output config.pb

configtxlator proto_encode --input modified_config.json --type common.Config --output modified_config.pb

configtxlator compute_update --channel_id example --original config.pb --updated modified_config.pb --output ${ORG}_update.pb

configtxlator proto_decode --input ${ORG}_update.pb --type common.ConfigUpdate | jq . > ${ORG}_update.json

echo '{"payload":{"header":{"channel_header":{"channel_id":"example","type":2}},"data":{"config_update":'$(cat ${ORG}_update.json)'}}}' | jq . > ${ORG}_update_in_eveloped.json

configtxlator proto_encode --input ${ORG}_update_in_eveloped.json --type common.Envelope --output ${ORG}_update_in_eveloped.pb

peer channel signconfigtx -f ${ORG}_update_in_eveloped.pb

peer channel update -f ${ORG}_update_in_eveloped.pb -c example -o orderer.example.com:7050